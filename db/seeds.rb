# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# === ZWIERZE 1 ==========
Animal.create(
  name: 'Lew afrykański',
  latin: 'Panthera leo',
  division: 'Ssaki',
  desc: 'Mięsożerny gatunek ssaka lądowego z rodziny kotowatych,
        drugi po tygrysie – co do wielkości – wśród czterech
        ryczących wielkich kotów. Jedyny kot żyjący w zorganizowanych
        grupach socjalnych, zaliczany do tzw. wielkiej piątki Afryki
        – pięciu najbardziej niebezpiecznych zwierząt afrykańskich
        (słoń, nosorożec, bawół, lew i lampart).'
)

# === ZWIERZE 2 ==========
Animal.create(
  name: 'Słoń indyjski',
  latin: 'Elephas maximus',
  division: 'Ssaki',
  desc: 'Gatunek ssaka z rzędu trąbowców, największe współcześnie
        żyjące zwierzę lądowe. Jeden z trzech żyjących gatunków
        rodziny słoniowatych, zamieszkujący lasy i zarośla Azji Południowej
        i Południowo-Wschodniej.'
)

# === ZWIERZE 3 ==========
Animal.create(
  name: 'Żubr europejski',
  latin: 'Bison bonasus',
  division: 'Ssaki',
  desc: 'Gatunek łożyskowca z rodziny wołowatych, rzędu parzystokopytnych.
        W 2013 roku światowa liczebność gatunku wynosiła 5249 osobników,
        z czego 1623 przebywało w hodowlach zamkniętych, a 3626 żyło
        w wolnych i w półwolnych populacjach.'
)

# === ZWIERZE 4 ==========
Animal.create(
  name: 'Żółw błotny',
  latin: 'Emys orbicularis',
  division: 'Gady',
  desc: 'Gatunek żółwi z rodziny żółwi błotnych z podrzędu żółwi
        skrytoszyjnych. Jedyny gatunek żółwia żyjący naturalnie
        w Polsce. W środowisku naturalnym żyje powyżej 100 lat.'
)

# === ZWIERZE 5 ==========
Animal.create(
  name: 'Bocian biały',
  latin: 'Ciconia ciconia',
  division: 'Ptaki',
  desc: 'Gatunek dużego ptaka brodzącego z rodziny bocianów (Ciconiidae).
        Jego upierzenie jest głównie białe, z czarnymi piórami na skrzydłach.
        Dorosłe ptaki mają długie czerwone nogi oraz długie spiczasto
        zakończone czerwone dzioby i mierzą średnio 100–115 cm od czubka
        dzioba do końca ogona, ze skrzydłami o rozpiętości 155–215 cm.'
)

# === ZWIERZE 6 ==========
Animal.create(
  name: 'Bóbr europejski',
  latin: 'Castor fiber',
  division: 'Ssaki',
  desc: 'Gatunek ziemnowodnego gryzonia z rodziny bobrowatych (Castoridae).
        Uważa się go za największego gryzonia Eurazji: masa ciała dorosłego
        osobnika dochodzi do 29 kg, a długość ciała do 110 cm. Jest zwierzęciem
        silnie terytorialnym, rodzinnym i zasadniczo monogamicznym; wiedzie
        nocny tryb życia.'
)

# === ZWIERZE 7 ==========
Animal.create(
  name: 'Jeż zachodni',
  latin: 'Erinaceus europaeus',
  division: 'Ssaki',
  desc: 'Gatunek ssaka łożyskowego z rodziny jeżowatych (Erinaceidae).
        Występuje w klimacie umiarkowanym na terenie od zachodniej Europy
        po zachodnią Polskę, Skandynawię i północno-zachodni obszar
        europejskiej części Rosji. Rzadko spotykany na północ od 60°N.
        Został introdukowany w Nowej Zelandii.'
)

# === ZWIERZE 8 ==========
Animal.create(
  name: 'Gacek wielkouch',
  latin: 'Plecotus auritus',
  division: 'Ssaki',
  desc: 'Gatunek ssaka z rzędu nietoperzy. Wraz z gackiem szarym stanowi
        trudną do rozróżnienia parę gatunków. Do około roku 1960 gatunków
        tych nie rozróżniano, używając polskiej nazwy gacek wielkouch.'
)

# === ZWIERZE 9 ==========
Animal.create(
  name: 'Świstak amerykański',
  latin: 'Marmota monax',
  division: 'Ssaki',
  desc: 'Gatunek dużego gryzonia z rodziny wiewiórkowatych (Scuridae),
        największy z przedstawicieli rodzaju Marmota. W przeciwieństwie
        do większości przedstawicieli tego rodzaju jest zwierzęciem nizinnym.
        Zamieszkuje tereny Stanów Zjednoczonych i Kanady.'
)

# === ZWIERZE 10 ==========
Animal.create(
  name: 'Kot domowy',
  latin: 'Felis catus',
  division: 'Ssaki',
  desc: 'Udomowiony gatunek ssaka z rzędu drapieżnych z rodziny kotowatych.
        Koty zostały udomowione około 9500 lat temu i są obecnie
        najpopularniejszymi zwierzętami domowymi na świecie. Gatunek
        prawdopodobnie pochodzi od kota nubijskiego, przy czym w Europie
        krzyżował się ze żbikiem. Jest uznawany za gatunek inwazyjny.'
)

# === ZWIERZE 11 ==========
Animal.create(
  name: 'Gołąb miejski',
  latin: 'Columba livia f. urbana',
  division: 'Ptaki',
  desc: 'Wywodzi się od udomowionego gołębia skalnego. Populacja
        gołębia miejskiego składa się z osobników zbiegłych z hodowli,
        które przystosowały się do życia w miastach.'
)

# === ZWIERZE 12 ==========
Animal.create(
  name: 'Hipopotam nilowy',
  latin: 'Hippopotamus amphibius',
  division: 'Ssaki',
  desc: 'Gatunek dużego, przeważnie roślinożernego ssaka,
        należącego do rodziny hipopotamowatych (Hippopotamidae),
        w obrębie której wyróżnia się dwa współcześnie żyjące gatunki
        (drugim jest dużo mniejszy hipopotam karłowaty). Hipopotam
        nilowy jest jedynym żyjącym przedstawicielem rodzaju Hippopotamus.'
)

# === ZWIERZE 13 ==========
Animal.create(
  name: 'Żyrafa kenijska',
  latin: 'Giraffa camelopardalis tippelskirchi',
  division: 'Ssaki',
  desc: 'Podgatunek żyrafy parzystokopytny ssaka roślinożernego
        oraz najwyższy gatunek z obecnie żyjących ssaków lądowych.
        Zamieszkuje afrykańskie sawanny południowej Kenii oraz niemal
        całej Tanzanii. W Kenii zamieszkuje parki: Masai Mara,
        Amboseli oraz Tsavo West.'
)

# === ZWIERZE 14 ==========
Animal.create(
  name: 'Kątnik domowy',
  latin: 'Tegenaria domestica',
  division: 'Pajęczaki',
  desc: 'Gatunek pająka z rodziny lejkowcowatych. Pająk ten
        ma karapaks o przyciemnionym brzegu i z dwoma symetrycznymi,
        podłużnymi pasami na grzbiecie. Przednia krawędź jego
        szczękoczułków ma 3, a tylna 3–4 zęby. Wzór na sternum
        składa się z jasnej przepaski środkowej i trzech par symetrycznie
        rozmieszczonych jasnych kropek.'
)

# === ZWIERZE 15 ==========
Animal.create(
  name: 'Szczupak pospolity',
  latin: 'Esox lucius',
  division: 'Ryby',
  desc: 'Gatunek szeroko rozprzestrzenionej, drapieżnej ryby
        z rodziny szczupakowatych (Esocidae). Jego okołobiegunowy
        zasięg występowania jest największym naturalnym zasięgiem
        ryb wyłącznie słodkowodnych.'
)

# === ZWIERZE 16 ==========
Animal.create(
  name: 'Dorsz atlantycki',
  latin: 'Gadus morhua',
  division: 'Ryby',
  desc: 'Drapieżna ryba morska, największy z przedstawicieli
        rodziny dorszowatych; gatunek wędrowny występujący w północnej
        części Oceanu Atlantyckiego i w morzach północnej Europy.'
)

# === ZWIERZE 17 ==========
Animal.create(
  name: 'Żmija nosoroga',
  latin: 'Vipera ammodytes',
  division: 'Gady',
  desc: 'Gatunek jadowitego węża z rodziny żmijowatych. Jej
        rozmieszczenie obejmuje cieplejsze rejony Europy (południowa
        Austria, północne Włochy, Półwysep Bałkański, Cyklady,
        Azja Mniejsza do Przedkaukazia).'
)

# === ZWIERZE 18 ==========
Animal.create(
  name: 'Salamandra plamista',
  latin: 'Salamandra salamandra',
  division: 'Płazy',
  desc: 'Gatunek płaza ogoniastego z rodziny salamandrowatych
        o charakterystycznym wyglądzie i szerokim zasięgu występowania.
        Prowadzi samotniczy, drapieżny tryb życia. Wyróżnia się
        liczne podgatunki. W Polsce podlega ochronie częściowej.'
)

# === ZWIERZE 19 ==========
Animal.create(
  name: 'Kumak górski',
  latin: 'Bombina variegata',
  division: 'Płazy',
  desc: 'Gatunek płaza bezogonowego z rodziny kumakowatych
        (dawniej zaliczany do ropuszkowatych), blisko spokrewniony
        z kumakiem nizinnym. Cechuje się brązową grzbietową stroną
        ciała i kontrastującą z nią stroną brzuszną, pokrytą
        ostrzegawczym deseniem żółci i czerni. Występuje w Europie.
        Zasiedla różnorodne środowiska. Rozmnaża się w małych, często
        istniejących krótkotrwale zbiornikach wodnych, niekiedy
        zanieczyszczonych.'
)

# === ZWIERZE 20 ==========
Animal.create(
  name: 'Szerszeń europejski',
  latin: 'Vespa crabro',
  division: 'Owady',
  desc: 'Gatunek owada z rodziny osowatych (Vespidae),
        z grupy żądłówek, największy z osowatych występujących
        w Europie Środkowej, pospolity na terenie całej Polski,
        zwłaszcza w pobliżu terenów zamieszkiwanych przez ludzi.
        Żywi się owadami, owocami oraz sokami niektórych drzew.
        Jego użądlenie może być niebezpieczne dla osób uczulonych
        na jad owadów błonkoskrzydłych. Jest gatunkiem typowym
        rodzaju Vespa.'
)
