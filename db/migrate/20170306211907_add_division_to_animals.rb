class AddDivisionToAnimals < ActiveRecord::Migration[5.0]
  def change
    add_column :animals, :division, :string
  end
end
