class AddLatinToAnimals < ActiveRecord::Migration[5.0]
  def change
    add_column :animals, :latin, :string
  end
end
