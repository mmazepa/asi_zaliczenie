# Mariusz Mazepa (zaliczenie) #

** Heroku: ** https://mmazepa.herokuapp.com/

| **Ruby**     | **Rails**    | **Baza**                  | **Framework** |
| -------------|--------------|---------------------------|---------------|
| 2.4.0        | 5.0.2        | **Lokalnie:** SQLite3     | Bootstrap     |
|              |              | **Heroku:** PostgreSQL    |               |

Niniejsza aplikacja będzie wyświetlać dane z bazy zwierząt oraz umożliwiać zarządzanie nimi (dodawanie, edycja, usuwanie).

## Użyte gemy ##

| L.P. | Nazwa gemu | Opis gemu                                                   |
| -----|------------|-------------------------------------------------------------|
| 1    | Paperclip  | Przesyłanie plików, na przykład obrazków. **(faza testów)** |
