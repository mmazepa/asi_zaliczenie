Rails.application.routes.draw do
  root :to => "animals#index"
  resources :animals
   get '/about' => "about#index"
   post '/animals/:id/edit' => "animals#update"
end
